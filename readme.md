# Propel module

## Configuration

```
extensions:
    propel: Slts\Propel\DI\PropelExtension
    
propel:
    config-dir: '%appDir%/config/' # path to dir with propel.yml(.dist) or path to specific conf file
    debugger: true # if it is true, it overrides propel log methods with logging into Tracy Bar, otherwise it let logging from propel configuration do the job
```

## Robo
You can add preconfigured Robo commands (currently there is a only one and it not supports all features `bin/propel` has)
```
class RoboFile extends \Robo\Tasks
{
    use \Slts\Propel\Robo\PropelCommands;
}
```
It runs basic propel commands with option --config-dir which is annoying to write if you have moved conf files.

Default config dir is set to app/config. If you want to change that, you can easily override the function propel() with your new
```
use \Slts\Propel\Robo\PropelCommands;
/**
 * @description Extended wrapper around ./bin/propel command with limited functionality
 * @param array $commands
 * @param array $opts
 */
public function propel(array $commands, $opts = ['config-dir' => 'app/my/hidden/conf_dir'])
{
    $this->propelCommand($commands, $opts);
}
```
