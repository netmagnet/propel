<?php

namespace Slts\Propel\Exception;

use Exception;

class PropelConfigurationException extends Exception
{

    public static function missing($path)
    {
        return new static("Propel configuration was not found in {$path}");
    }

}