<?php

namespace Slts\Propel\Log;

use Propel\Runtime\Propel;
use Psr\Log\LoggerInterface;

class PropelLogger implements LoggerInterface
{

    /**
     * @var array
     */
    private array $queries = [];

    /**
     * @var string
     */
    private string $connectionName;


    /**
     * Propel2Logger constructor.
     *
     * @param string $connectionName
     */
    public function __construct(string $connectionName)
    {
        $this->connectionName = $connectionName;
    }

    /**
     * @return string
     */
    public function getConnectionName(): string
    {
        return $this->connectionName;
    }

    /**
     * @return array
     */
    public function getQueries(): array
    {
        return $this->queries;
    }

    /**
     * @return bool
     */
    public function hasQueries(): bool
    {
        return (bool)$this->queries;
    }

    /**
     * @inheritdoc
     */
    public function emergency(string|\Stringable $message, array $context = []): void
    {
        $this->log(Propel::LOG_EMERG, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function alert(string|\Stringable $message, array $context = []): void
    {
        $this->log(Propel::LOG_ALERT, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function critical(string|\Stringable $message, array $context = []): void
    {
        $this->log(Propel::LOG_CRIT, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function error(string|\Stringable $message, array $context = []): void
    {
        $this->log(Propel::LOG_ERR, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function warning(string|\Stringable $message, array $context = []): void
    {
        $this->log(Propel::LOG_WARNING, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function notice(string|\Stringable $message, array $context = []): void
    {
        $this->log(Propel::LOG_NOTICE, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function info(string|\Stringable $message, array $context = []): void
    {
        $this->log(Propel::LOG_INFO, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function debug(string|\Stringable $message, array $context = []): void
    {
        $this->log(Propel::LOG_DEBUG, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function log($level, string|\Stringable $message, array $context = []): void
    {
        $this->queries[] = [$level, $message, $context];
    }

}