<?php

namespace Slts\Propel\DI;

use Nette\DI\Helpers;
use Nette\Utils\Finder;
use Slts\Propel\Exception\PropelConfigurationException;
use Slts\Propel\Panel\PropelPanel;
use Nette\DI\CompilerExtension;
use Propel\Common\Config\ConfigurationManager;


class PropelExtension extends CompilerExtension
{

    /**
     * @var array $defaults
     */
    private $defaults = [
        'config-dir' => '%appDir%/config/',
        'debugger' => '%debugMode%',
        'projectRootDir' => '%appDir%/../'
    ];

    /**
     * @var ConfigurationManager $propelConfigManager
     */
    private $propelConfigManager;


    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();
        $expandedParams = Helpers::expand($this->defaults, $builder->parameters);
        $this->validateConfig($expandedParams);
        $config = $this->getConfig();

        if (!file_exists($config['config-dir'])) {
            throw PropelConfigurationException::missing($config['config-dir']);
        }
        if (is_dir($config['config-dir'])) {
            $files = [];
            $baseName = ConfigurationManager::CONFIG_FILE_NAME . '.';
            foreach (['php', 'inc', 'ini', 'properties', 'yaml', 'yml', 'xml', 'json'] as $ext) {
                $files[] = $baseName . $ext;
                $files[] = $baseName . $ext . '.dist';
            }
            $dirs = array_filter([$config['config-dir'] . '/config', $config['config-dir'] . '/conf'], 'is_dir');
            $dirs[] = $config['config-dir'];
            if (! count(iterator_to_array(Finder::findFiles($files)->in($dirs)))) {
                throw PropelConfigurationException::missing($config['config-dir']);
            }
        }

        $this->propelConfigManager = new ConfigurationManager($config['config-dir']);
    }

    public function beforeCompile()
    {
        $this->setupServiceContainer();

        $builder = $this->getContainerBuilder();
        if ($builder->hasDefinition('application')) {
            $builder
                ->getDefinition('application')
                ->addSetup(
                    new \Nette\DI\Definitions\Statement(<<<STM
\$service->onStartup[] = function() {
    //invoke database init
    \$this->getService(?);
};
STM,
                        [$this->prefix('serviceContainer')]
                    )
                )
            ;
        }
    }

    private function setupServiceContainer(): void
    {
        $builder = $this->getContainerBuilder();
        $config = $this->getConfig();

        $scDef = $builder
            ->addDefinition($this->prefix('serviceContainer'))
            ->setFactory('Propel\Runtime\Propel::getServiceContainer')
        ;

        $rawConfFilePath = rtrim($config['projectRootDir'], '/') . '/' . rtrim($this->propelConfigManager->getSection('paths')['phpConfDir'], '/') . '/config.php';
        $configFile = realpath($rawConfFilePath);
        if (false === $configFile) {
            throw new \RuntimeException("Config file \"{$rawConfFilePath}\" not found");
        }
        $scDef
            ->addSetup(
                new \Nette\DI\Definitions\Statement(
                    'require_once ?',
                    [$configFile]
                )
            )
        ;

        if ($config['debugger'] && $builder->hasDefinition('tracy.bar')) {
            $scDef->addSetup(
                new \Nette\DI\Definitions\Statement(<<<STM
\$panel = new Slts\Propel\Panel\PropelPanel();
?->addPanel(\$panel, "propelPanel");
STM,
                [$builder->getDefinition('tracy.bar')]
                )
            );
            $scDef->addSetup(
                new \Nette\DI\Definitions\Statement(<<<STM
\$l0 = new Slts\Propel\Log\PropelLogger(?);
\$service->setLogger(?, \$l0);
\$panel->addLogger(\$l0);
STM,
                    ['defaultLogger', 'defaultLogger']
                )
            );

            $counter = 1;
            $options['connections'] = $this->propelConfigManager->getConnectionParametersArray();
            foreach ($options['connections'] as $name => $conn) {
                $scDef->addSetup(
                    new \Nette\DI\Definitions\Statement(<<<STM
\$l{$counter} = new Slts\Propel\Log\PropelLogger(?);
\$service->setLogger(?, \$l{$counter});
\$panel->addLogger(\$l{$counter});
\$service->getConnection(?)->useDebug();
STM,
                        [$name, $name, $name]
                    )
                );
                $counter++;
            }
        }
    }
}
