<?php

namespace Slts\Propel\Robo;

use Robo\Application;

trait PropelCommands
{

    /**
     * @description Basic wrapper around ./bin/propel command with limited functionality
     *
     * @param array $commands
     * @param array $opts
     */
    public function propel(array $commands, array $opts = ['bin-dir' => 'bin', 'config-dir' => 'app/config'])
    {
        $task = $this->taskPropel($commands, $opts);
        $task->run();
    }

    protected function taskPropel(
        array $commands,
        array $opts = ['bin-dir' => 'bin', 'config-dir' => 'app/config']
    ) {
        $platformPrefix = (0 === stripos(PHP_OS, 'WIN')) ? 'sh ' : '';

        $cmd = implode(' ', $commands);

        $binDir = rtrim(isset($opts['bin-dir']) ? $opts['bin-dir'] : 'vendor/bin', '\/');
        unset($opts['bin-dir']);
        $propelBinary = "{$binDir}/propel";

        /** @var Application $app */
        $app = $this->getContainer()->get('application');
        $defaultOptions = $app->getDefinition()->getOptionDefaults();
        $cmp = function ($provided, $defaults) use (&$cmp) {
            if (is_array($provided)) {
                $diff = array_udiff_assoc($provided, $defaults, $cmp);
                return $diff ? 1 : 0;
            }

            return $provided === $defaults ? 0 : 1;
        };
        $filteredOpts = array_udiff_assoc($opts, $defaultOptions, $cmp);

        $task = $this->taskExec("{$platformPrefix}{$propelBinary}");
        if ($cmd) {
            $task->arg($cmd);
        }
        foreach ($filteredOpts as $kOpt => $vOpt) {
            if (is_array($vOpt)) {
                foreach ($vOpt as $vaOpt) {
                    $task->arg("--{$kOpt}={$vaOpt}");
                }
                continue;
            }
            $task->arg("--{$kOpt}={$vOpt}");
        }

        return $task;
    }
}
