<?php

namespace Slts\Propel\Connection;

use Propel\Runtime\Connection\ProfilerConnectionWrapper;
use Propel\Runtime\Connection\StatementWrapper;

class ImprovedProfilerConnectionWrapper extends ProfilerConnectionWrapper
{
    protected function createStatementWrapper($sql): StatementWrapper
    {
        return new ImprovedProfilerStatementWrapper($sql, $this);
    }

}
