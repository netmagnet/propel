<?php

namespace Slts\Propel\Connection;

use Propel\Runtime\Connection\ProfilerStatementWrapper;

class ImprovedProfilerStatementWrapper extends ProfilerStatementWrapper
{
    public function execute(?array $inputParameters = null): bool
    {
        $return = $this->statement->execute($inputParameters);
        if ($this->connection->useDebug) {
            $sql = $this->getExecutedQueryString($this->prepareInputParametersToLog($inputParameters));
            $this->connection->log($sql);
            $this->connection->setLastExecutedQuery($sql);
            $this->connection->incrementQueryCount();
        }

        return $return;
    }

    protected function prepareInputParametersToLog($input_parameters = null)
    {
        $bound = [];
        foreach ($this->boundValues as $key => $param) {
            $bound[substr($key, 1)] = is_string($param) ? trim($param, "'") : $param;
        }
        return array_merge((array)$input_parameters, $bound);
    }

    public function getExecutedQueryString(?array $inputParameters = null): string
    {
        $parameters = null === $inputParameters ? [] : $inputParameters;
        $i = 0;
        $result = preg_replace_callback(
            '/\?|((?<!:):[a-z0-9_]+)/i',
            function ($matches) use ($parameters, &$i) {
                $key = substr($matches[0], 1);
                if (!array_key_exists($i, $parameters) && (false === $key || !array_key_exists($key, $parameters))) {
                    return $matches[0];
                }

                $value = array_key_exists($i, $parameters) ? $parameters[$i] : $parameters[$key];
                $result = ImprovedProfilerStatementWrapper::escapeFunction($value);
                $i++;

                return $result;
            },
            $this->statement->queryString
        );

        return $result;
    }

    /**
     * Escape parameters of a SQL query
     * DON'T USE THIS FUNCTION OUTSIDE ITS INTENDED SCOPE
     *
     * @internal
     *
     * @param mixed $parameter
     *
     * @return string
     */
    public static function escapeFunction($parameter)
    {
        $result = $parameter;

        switch (true) {
            case is_string($result):
                $result = "'".addslashes($result)."'";
                break;

            case is_array($result):
                foreach ($result as &$value) {
                    $value = static::escapeFunction($value);
                }

                $result = implode(', ', $result);
                break;

            case is_object($result):
                $result = addslashes((string) $result);
                break;

            case null === $result:
                $result = 'NULL';
                break;

            case is_bool($result):
                $result = $result ? '1' : '0';
                break;
        }

        return $result;
    }
}
