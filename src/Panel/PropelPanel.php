<?php

namespace Slts\Propel\Panel;


use Slts\Propel\Log\PropelLogger;
use Propel\Runtime\Propel;
use Tracy\IBarPanel;

class PropelPanel implements IBarPanel
{

    private $loggers = [];

    /**
     * Renders HTML code for custom tab.
     * @return string
     */
    public function getTab()
    {
        ob_start();
        require __DIR__ . '/templates/Propel.tab.php';

        return ob_get_clean();
    }

    /**
     * Renders HTML code for custom panel.
     * @return string
     */
    public function getPanel()
    {
        ob_start();
        require __DIR__ . '/templates/Propel.panel.php';

        return ob_get_clean();
    }

    /**
     * @param PropelLogger $logger
     */
    public function addLogger(PropelLogger $logger)
    {
        $this->loggers[$logger->getConnectionName()] = $logger;
    }

    /**
     * @param string $name
     *
     * @return PropelLogger|null
     */
    public function getLogger($name)
    {
        return isset($this->loggers[$name]) ? $this->loggers[$name] : null;
    }

    private function priorityToText($priority)
    {
        switch ($priority) {
            case Propel::LOG_EMERG:
                return '<span style="color: red">Emergency</span>';
            case Propel::LOG_ALERT:
                return '<span style="color: red">Alert</span>';
            case Propel::LOG_CRIT:
                return '<span style="color: red">Critical</span>';
            case Propel::LOG_ERR:
                return '<span style="color: red">Error</span>';
            case Propel::LOG_WARNING:
                return '<span style="color: orange">Warning</span>';
            case Propel::LOG_NOTICE:
                return '<span style="color: green">Notice</span>';
            case Propel::LOG_INFO:
                return '<span style="color: blue">Info</span>';
            case Propel::LOG_DEBUG:
                return '<span style="color: grey">Debug</span>';
            default:
                return '<span style="color: grey">Custom</span>';
        }
    }

    private function formatMessage($message)
    {
        $rows = explode(' | ', $message);

        return implode('<br>', $rows);
    }
}