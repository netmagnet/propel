<h1>Propel 2 logs</h1>
<div class="tracy-inner">
    <?php 
    /** @var \Slts\Propel\Panel\PropelPanel $this */
    /** @var \Slts\Propel\Log\PropelLogger $logger */
    foreach ($this->loggers as $name => $logger) { ?>
        <table style="width: 100%;">
            <thead>
            <tr>
                <th colspan="2">Connection: <strong><?= $name; ?></strong></th>
                <th>Queries: <?= count($logger->getQueries()); ?></th>
            </tr>
            <?php if ($logger->hasQueries()) { ?>
                <tr>
                    <th>Priority</th>
                    <th>SQL</th>
                    <th>Context</th>
                </tr>
            <?php } ?>
            </thead>

            <tbody>
            <?php if ($logger->hasQueries()) { ?>
                <?php foreach ($logger->getQueries() as $query) { ?>
                    <tr>
                        <td><?= $this->priorityToText($query[0]) ?></td>
                        <td><?= $this->formatMessage($query[1]) ?></td>
                        <td><?= empty($query[2]) ? '' : \Tracy\Debugger::dump($query[2], true); ?></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="3">
                        <h2>No queries executed</h2>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>
</div>