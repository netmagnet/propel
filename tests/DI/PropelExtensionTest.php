<?php

namespace Slts\Propel\Tests\DI;

use FilesystemIterator;
use Nette\DI\Compiler;
use Nette\DI\Container;
use Nette\DI\ContainerFactory;
use Nette\DI\ContainerLoader;
use PHPUnit\Framework\TestCase;
use Propel\Runtime\Connection\ConnectionWrapper;
use Propel\Runtime\Propel;
use Propel\Runtime\ServiceContainer\ServiceContainerInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Slts\Propel\DI\PropelExtension;
use Slts\Propel\Exception\PropelConfigurationException;
use Slts\Propel\Log\PropelLogger;
use Slts\Propel\Panel\PropelPanel;
use Tracy\Bridges\Nette\TracyExtension;

class PropelExtensionTest extends TestCase
{
    
    private static function getCacheDirPath()
    {
        return __DIR__ . '/../temp/cache';
    }

    public static function setUpBeforeClass(): void
    {
        $cacheDir = self::getCacheDirPath();
        if (!is_dir($cacheDir)) {
            mkdir($cacheDir, 0777, true);
        }
    }

    public static function tearDownAfterClass(): void
    {
        $dir = self::getCacheDirPath() . '/../';
        $di = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
        $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($ri as $file) {
            $file->isDir() ? rmdir($file) : unlink($file);
        }
        
        @rmdir($dir);
    }

    public function setUp(): void
    {
        // must be because of Nette screwed code
//        PHPUnit_Framework_Error_Deprecated::$enabled = false;
    }

    public function tearDown(): void
    {
        $dir = self::getCacheDirPath();
        $di = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
        $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($ri as $file) {
            $file->isDir() ? rmdir($file) : unlink($file);
        }
    }
    
    private function createContainerFactory(array $config)
    {
        $factory = new ContainerFactory(null);
        $factory->autoRebuild = true;
        $factory->class = $config['parameters']['container']['class'];
        $factory->config = $config;
        $factory->configFiles = [];
        $factory->tempDirectory = self::getCacheDirPath() . '/Nette.Configurator';
        if (!is_dir($factory->tempDirectory)) {
            mkdir($factory->tempDirectory);
        }

        $factory->onCompile[] = function (ContainerFactory $factory, Compiler $compiler, $config){
            $compiler->addExtension('propel', new PropelExtension);
            $factory->parentClass = $config['parameters']['container']['parent'];
        };

        return $factory;
    }

    private function createOldContainer(array $config)
    {
        $cFactory = $this->createContainerFactory($config);
        $container = $cFactory->create();
        $container->initialize();

        return $container;
    }

    private function loadContainer(array $config)
    {
        $loader = new ContainerLoader(
            $this->getCacheDirPath() . '/Nette.Configurator',
            $config['parameters']['debugMode']
        );
        $class = $loader->load(
            function (Compiler $compiler) use ($config) {
                $compiler->addConfig(['parameters' => $config['parameters']]);
                $compiler->addConfig($config);
                $compiler->addExtension('propel', new PropelExtension);
                $compiler->addExtension('tracy', new TracyExtension);
                $classes = $compiler->compile();
                return implode("\n", []) . "\n\n" . $classes;
            },
            [$config['parameters'], [], PHP_VERSION_ID - PHP_RELEASE_VERSION]
        );
        return $class;
    }

    private function createNewContainer(array $params)
    {
        $class = $this->loadContainer($params);
        $container = new $class();
        $container->initialize();
        return $container;
    }

    private function createContainer(array $config, $debugMode)
    {
        $config = array_merge($this->getDefaultParams($debugMode), $config);
        if (class_exists('\Nette\DI\ContainerFactory')) {
            return $this->createOldContainer($config);
        }
        
        return $this->createNewContainer($config);
    }

    private function getDefaultParams($debugMode)
    {
        $trace = debug_backtrace(PHP_VERSION_ID >= 50306 ? DEBUG_BACKTRACE_IGNORE_ARGS : false);
        return [
            'parameters' => [
                'appDir' => isset($trace[1]['file']) ? dirname($trace[1]['file']) : null,
                'wwwDir' => isset($_SERVER['SCRIPT_FILENAME'])
                    ? dirname(realpath($_SERVER['SCRIPT_FILENAME']))
                    : null,
                'debugMode' => $debugMode,
                'productionMode' => !$debugMode,
                'environment' => $debugMode ? 'development' : 'production',
                'consoleMode' => PHP_SAPI === 'cli',
                'container' => [
                    'class' => 'SystemContainer' . uniqid(),
                    'parent' => 'Nette\DI\Container',
                ],
                'testDir' => __DIR__ . '/../',
            ],
        ];
    }

    public function testConfDirParameter()
    {
        $container = $this->createContainer([
            'propel' => [
                'config-dir' => '%testDir%/config/',
            ],
        ], false);

        $container = $this->createContainer([
            'propel' => [
                'config-dir' => '%testDir%/',
            ],
        ], false);

        $container = $this->createContainer([
            'propel' => [
                'config-dir' => '%testDir%/config/propel.yml.dist',
            ],
        ], false);

        $this->expectException(PropelConfigurationException::class);
        $container = $this->createContainer([
            'propel' => [
                'config-dir' => '/my/bad/path',
            ],
        ], false);

        $container = $this->createContainer([
            'propel' => [
                'config-dir' => '%testDir%/config/propel.yml',
            ],
        ], false);
    }

    public function testRegisteredBasicServices()
    {
        $container = $this->createContainer([
            'propel' => [
                'config-dir' => '%testDir%/config/',
            ],
        ], false);
        $serviceContainer = $container->getByType(ServiceContainerInterface::class);
        $this->assertInstanceOf(ServiceContainerInterface::class, $serviceContainer);
    }

    /**
     * It is unfortunately necessary to run all those assertion and creating of Container in that order
     * because of static Tracy and its bar. Since there is no way to clean up Bar after creating, the successful
     * add of panel to it must be called last.
     *
     * Or give me some help cause I may be dumb.
     */
    public function testRegisteredTracyLogger()
    {
        /** @var Container $container */
        $container = $this->createContainer([
            'propel' => [
                'config-dir' => '%testDir%/config/',
            ],
        ], false);
        $nullPanel = $container->getByName('tracy.bar')->getPanel('propelPanel');
        $this->assertEquals(null, $nullPanel);

        $container = $this->createContainer([
            'propel' => [
                'config-dir' => '%testDir%/config/',
                'debugger' => false,
            ],
        ], true);
        /** @var PropelPanel $panel */
        $container->getByType(ServiceContainerInterface::class);
        $panel = $container->getByName('tracy.bar')->getPanel('propelPanel');
        $this->assertNull($panel);

        $container = $this->createContainer([
            'propel' => [
                'config-dir' => '%testDir%/config/',
            ],
        ], true);
        /** @var PropelPanel $panel */
        $container->getByType(ServiceContainerInterface::class);
        $panel = $container->getByName('tracy.bar')->getPanel('propelPanel');
        $this->assertInstanceOf(PropelPanel::class, $panel);

        $logger = $panel->getLogger('defaultLogger');
        $this->assertInstanceOf(PropelLogger::class, $logger);

        $logger = $panel->getLogger('default');
        $this->assertInstanceOf(PropelLogger::class, $logger);

        $uLogger = $panel->getLogger('unknown');
        $this->assertNull($uLogger);
        
        /** @var ConnectionWrapper $connection */
        $connection = Propel::getServiceContainer()->getConnection('default');
        $conLogger = $connection->getLogger();
        $this->assertInstanceOf(PropelLogger::class, $conLogger);
    }

}