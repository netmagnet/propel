<?php

namespace Slts\Propel\Tests\Log;

use PHPUnit\Framework\TestCase;
use Propel\Runtime\Propel;
use Slts\Propel\Log\PropelLogger;

class PropelLoggerTest extends TestCase
{

    /**
     * @var PropelLogger
     */
    private $logger;

    public function setUp(): void
    {
        $this->logger = new PropelLogger('default');
    }

    public function testGetConnectionName()
    {
        $this->assertSame('default', $this->logger->getConnectionName());
    }
    
    /**
     * @dataProvider logMethodsProvider
     */
    public function testSpecialLogMethods($method, $message, $context, $expectedLevel)
    {
        $this->logger->{$method}($message, $context);
        $this->assertEquals([[$expectedLevel, $message, $context]], $this->logger->getQueries());
    }

    public function testLogMethod()
    {
        $this->assertSame([], $this->logger->getQueries());
        $this->assertFalse($this->logger->hasQueries());
        
        $this->logger->log('custom', 'Test log method', []);
        $this->assertEquals([['custom', 'Test log method', []]], $this->logger->getQueries());
        $this->assertTrue($this->logger->hasQueries());

        $this->logger->log('custom', 'Test more log method', []);
        $this->assertEquals([
            ['custom', 'Test log method', []],
            ['custom', 'Test more log method', []],
        ], $this->logger->getQueries());
        $this->assertTrue($this->logger->hasQueries());
    }

    public function logMethodsProvider()
    {
        return [
            ['emergency', 'Test emergency method', [], Propel::LOG_EMERG],
            ['alert', 'Test alert method', [], Propel::LOG_ALERT],
            ['critical', 'Test critical method', [], Propel::LOG_CRIT],
            ['error', 'Test error method', [], Propel::LOG_ERR],
            ['warning', 'Test warning method', [], Propel::LOG_WARNING],
            ['notice', 'Test notice method', [], Propel::LOG_NOTICE],
            ['info', 'Test info method', [], Propel::LOG_INFO],
            ['debug', 'Test debug method', [], Propel::LOG_DEBUG],
        ];
    }
}